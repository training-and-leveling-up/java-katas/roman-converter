package com.jgarfer.katas;

public class DecimalConverter {

    private static final int[] MASTER_NUMBERS = {500, 100, 50, 10, 5, 1};

    public String convertToRoman(int decimalNumber) {
        StringBuilder result = new StringBuilder();

        for (int i = 0; i < MASTER_NUMBERS.length && decimalNumber > 0; i++) {
            int masterNumber = MASTER_NUMBERS[i];

            while (decimalNumber - masterNumber >= 0) {
                result.append(convertToBasicRomanNumber(masterNumber));
                decimalNumber -= masterNumber;
            }

            if (decimalNumber > 0) {
                if (i + 2 < MASTER_NUMBERS.length && masterNumber - MASTER_NUMBERS[i + 2] <= decimalNumber) {
                    result.append(convertToBasicRomanNumber(MASTER_NUMBERS[i + 2]));
                    result.append(convertToBasicRomanNumber(masterNumber));
                    decimalNumber -= masterNumber - MASTER_NUMBERS[i + 2];
                } else if (masterNumber - 1 <= decimalNumber) {
                    if (masterNumber - 1 == decimalNumber) {
                        result.append(convertToBasicRomanNumber(1));
                    }
                    result.append(convertToBasicRomanNumber(masterNumber));
                    decimalNumber -= decimalNumber;
                } else if (i + 1 < MASTER_NUMBERS.length &&
                        masterNumber - MASTER_NUMBERS[i + 1] == decimalNumber && decimalNumber > 10
                ) {
                    result.append(convertToBasicRomanNumber(MASTER_NUMBERS[i + 1]));
                    result.append(convertToBasicRomanNumber(masterNumber));
                    decimalNumber -= decimalNumber;
                }
            }
        }

        assert (decimalNumber == 0);

        return result.toString();
    }

    private String convertToBasicRomanNumber(int decimalNumber) {
        return switch (decimalNumber) {
            case 1 -> "i";
            case 5 -> "v";
            case 10 -> "x";
            case 50 -> "l";
            case 100 -> "c";
            case 500 -> "d";
            default -> "";
        };
    }
}
