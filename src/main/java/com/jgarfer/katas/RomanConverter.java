package com.jgarfer.katas;

public class RomanConverter {


    public int convertToDecimal(String romanNumber) {
        int decimal = 0;
        for (int pos = 0; pos < romanNumber.length(); pos++) {
            char c = romanNumber.charAt(pos);

            if (pos < romanNumber.length() - 1 && isRomanCharacterMinorThan(c, romanNumber.charAt(pos + 1))) {
                decimal -= calculateCharacterValue(c);
            } else {
                decimal += calculateCharacterValue(c);
            }
        }
        return decimal;
    }

    private int calculateCharacterValue(char character) {
        return switch (character) {
            case 'i' -> 1;
            case 'v' -> 5;
            case 'x' -> 10;
            case 'l' -> 50;
            case 'c' -> 100;
            case 'd' -> 500;
            case 'm' -> 1000;
            default -> 0;
        };
    }

    private boolean isRomanCharacterMinorThan(char x, char xPlus1) {
        return calculateCharacterValue(x) < calculateCharacterValue(xPlus1);
    }
}