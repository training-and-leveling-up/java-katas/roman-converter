package com.jgarfer.katas;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.assertEquals;

public class DecimalConverterTest {

    private DecimalConverter converter;

    @BeforeEach
    public void setUp() {
        this.converter = new DecimalConverter();
    }

    @Test
    public void testZero() {
        assertEquals("", converter.convertToRoman(0));
    }

    @Test
    public void testOne() {
        assertEquals("i", converter.convertToRoman(1));
    }

    @Test
    public void testTwo() {
        assertEquals("ii", converter.convertToRoman(2));
    }

    @Test
    public void testThree() {
        assertEquals("iii", converter.convertToRoman(3));
    }

    @Test
    public void testFour() {
        assertEquals("iv", converter.convertToRoman(4));
    }

    @Test
    public void testFive() {
        assertEquals("v", converter.convertToRoman(5));
    }

    @Test
    public void testSix() {
        assertEquals("vi", converter.convertToRoman(6));
    }

    @Test
    public void testSeven() {
        assertEquals("vii", converter.convertToRoman(7));
    }

    @Test
    public void testEight() {
        assertEquals("viii", converter.convertToRoman(8));
    }

    @Test
    public void testNine() {
        assertEquals("ix", converter.convertToRoman(9));
    }

    @Test
    public void testTen() {
        assertEquals("x", converter.convertToRoman(10));
    }

    @Test
    public void testEleven() {
        assertEquals("xi", converter.convertToRoman(11));
    }

    @Test
    public void testTwelve() {
        assertEquals("xii", converter.convertToRoman(12));
    }

    @Test
    public void testThirteen() {
        assertEquals("xiii", converter.convertToRoman(13));
    }

    @Test
    public void testFourteen() {
        assertEquals("xiv", converter.convertToRoman(14));
    }

    @Test
    public void testFifteen() {
        assertEquals("xv", converter.convertToRoman(15));
    }

    @Test
    public void testSixteen() {
        assertEquals("xvi", converter.convertToRoman(16));
    }

    @Test
    public void testSeventeen() {
        assertEquals("xvii", converter.convertToRoman(17));
    }

    @Test
    public void testEighteen() {
        assertEquals("xviii", converter.convertToRoman(18));
    }

    @Test
    public void testNineteen() {
        assertEquals("xix", converter.convertToRoman(19));
    }

    @Test
    public void testTwenty() {
        assertEquals("xx", converter.convertToRoman(20));
    }

    @Test
    public void testTwentyOne() {
        assertEquals("xxi", converter.convertToRoman(21));
    }

    @Test
    public void testTwentyTwo() {
        assertEquals("xxii", converter.convertToRoman(22));
    }

    @Test
    public void testTwentyThree() {
        assertEquals("xxiii", converter.convertToRoman(23));
    }

    @Test
    public void testTwentyFour() {
        assertEquals("xxiv", converter.convertToRoman(24));
    }

    @Test
    public void testTwentyFive() {
        assertEquals("xxv", converter.convertToRoman(25));
    }

    @Test
    public void testForty() {
        assertEquals("xl", converter.convertToRoman(40));
    }

    @Test
    public void testSixty() {
        assertEquals("lx", converter.convertToRoman(60));
    }

    @Test
    public void testNinety() {
        assertEquals("xc", converter.convertToRoman(90));
    }

    @Test
    public void testNinetyNine() {
        assertEquals("xcix", converter.convertToRoman(99));
    }

    @Test
    public void testOneHundred() {
        assertEquals("c", converter.convertToRoman(100));
    }

    @Test
    public void testOneHundredAndOne() {
        assertEquals("ci", converter.convertToRoman(101));
    }

    @Test
    public void testOneHundredAndNine() {
        assertEquals("cix", converter.convertToRoman(109));
    }
}