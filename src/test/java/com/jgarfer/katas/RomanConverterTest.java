package com.jgarfer.katas;

import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.assertEquals;

public class RomanConverterTest {

    @Test
    public void testBasic() {
        RomanConverter converter = new RomanConverter();
        assertEquals(0, converter.convertToDecimal(""));
    }

    @Test
    public void testI() {
        RomanConverter converter = new RomanConverter();
        assertEquals(1, converter.convertToDecimal("i"));
    }

    @Test
    public void testIi() {
        RomanConverter converter = new RomanConverter();
        assertEquals(2, converter.convertToDecimal("ii"));
    }

    @Test
    public void testIii() {
        RomanConverter converter = new RomanConverter();
        assertEquals(3, converter.convertToDecimal("iii"));
    }

    @Test
    public void testIv() {
        RomanConverter converter = new RomanConverter();
        assertEquals(4, converter.convertToDecimal("iv"));
    }

    @Test
    public void testV() {
        RomanConverter converter = new RomanConverter();
        assertEquals(5, converter.convertToDecimal("v"));
    }

    @Test
    public void testVi() {
        RomanConverter converter = new RomanConverter();
        assertEquals(6, converter.convertToDecimal("vi"));
    }

    @Test
    public void testVii() {
        RomanConverter converter = new RomanConverter();
        assertEquals(7, converter.convertToDecimal("vii"));
    }

    @Test
    public void testViii() {
        RomanConverter converter = new RomanConverter();
        assertEquals(8, converter.convertToDecimal("viii"));
    }

    @Test
    public void testIx() {
        RomanConverter converter = new RomanConverter();
        assertEquals(9, converter.convertToDecimal("ix"));
    }

    @Test
    public void testX() {
        RomanConverter converter = new RomanConverter();
        assertEquals(10, converter.convertToDecimal("x"));
    }

    @Test
    public void testFifty() {
        RomanConverter converter = new RomanConverter();
        assertEquals(50, converter.convertToDecimal("l"));
    }

    @Test
    public void testFortyEight() {
        RomanConverter converter = new RomanConverter();
        assertEquals(48, converter.convertToDecimal("xxxxviii"));
    }

    @Test
    public void testFortyNine() {
        RomanConverter converter = new RomanConverter();
        assertEquals(49, converter.convertToDecimal("il"));
    }

    @Test
    public void testFiftyTwo() {
        RomanConverter converter = new RomanConverter();
        assertEquals(52, converter.convertToDecimal("lii"));
    }

    @Test
    public void testEightyNine() {
        RomanConverter converter = new RomanConverter();
        assertEquals(89, converter.convertToDecimal("ixc"));
    }

    @Test
    public void testOneHundred() {
        RomanConverter converter = new RomanConverter();
        assertEquals(100, converter.convertToDecimal("c"));
    }

    @Test
    public void testNinety() {
        RomanConverter converter = new RomanConverter();
        assertEquals(90, converter.convertToDecimal("xc"));
    }

    @Test
    public void testFiveHundred() {
        RomanConverter converter = new RomanConverter();
        assertEquals(500, converter.convertToDecimal("d"));
    }

    @Test
    public void testFourHundredAndThirtySix() {
        RomanConverter converter = new RomanConverter();
        assertEquals(436, converter.convertToDecimal("cdxxxvi"));
    }

    @Test
    public void testOneThousand() {
        RomanConverter converter = new RomanConverter();
        assertEquals(1000, converter.convertToDecimal("m"));
    }
}